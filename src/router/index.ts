import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: ()=> import('../views/HomeView.vue'),
        menu: ()=> import ("@/components/menu/MainMenu.vue"),
        header:()=> import ("@/components/header/MainHeader.vue")
        
      },
      meta:{
        layout:"MainLayout"
      }
    },
    {
      path: '/about',
      name: 'about',
      components: {
        default: ()=> import ("@/views/AboutView.vue"),
        menu: ()=> import ("@/components/menu/AboutMenu.vue"),
        header:()=> import ("@/components/header/AboutHeader.vue")
        
      },
      meta:{
        layout:"FullLayout"
      }
    },
    {
      path: '/products',
      name: 'products',
      components: {
        default: ()=> import ("@/views/products/ProductView.vue"),
        menu: ()=> import ("@/components/menu/MainMenu.vue"),
        header:()=> import ("@/components/header/MainHeader.vue")
        
      },
      meta:{
        layout:"MainLayout"
      }
    },
    {
      path: '/products/full',
      name: 'products fullscreen',
      components: {
        default: ()=> import ("@/views/products/ProductView.vue"),
        menu: ()=> import ("@/components/menu/MainMenu.vue"),
        header:()=> import ("@/components/header/MainHeader.vue")
        
      },
      meta:{
        layout:"FullLayout"
      }
    },
    {
      path: '/users',
      name: 'users',
      components: {
        default: ()=> import ("@/views/users/UserView.vue"),
        menu: ()=> import ("@/components/menu/MainMenu.vue"),
        header:()=> import ("@/components/header/MainHeader.vue")
        
      },
      meta:{
        layout:"MainLayout"
      }
    }
  ]
})

export default router
