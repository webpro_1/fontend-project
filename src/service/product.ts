import type Product from "@/stores/types/Product";
import http from "./axios";
function getProducts() {
  return http.get("/products");
}
const saveProduct = (product:Product) => {
  return http.post("/products", product)
}

const updateProduct = (id:number,product:Product) => {
    return http.patch(`/products/${id}`, product)
    
}
const deleteProduct = (id:number) => {
    return http.delete(`/products/${id}`)
    
}

export default {deleteProduct,updateProduct, getProducts ,saveProduct};
