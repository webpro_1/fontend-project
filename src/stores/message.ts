import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useMessageStore = defineStore('message', () => {
  const isShow = ref(false);
  const message = ref('');
  const showError = (msg:string)=>{
    message.value = msg;
    isShow.value = true;

  }

return {isShow, message,showError}

})
