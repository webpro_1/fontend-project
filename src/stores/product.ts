import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "./types/Product";
import productService from "@/service/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductStore = defineStore("product", () => {
  const products = ref<Product[]>([]);
  const dialog = ref(false);
  const editProduct = ref<Product>({ name: "", price: 0 });
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editProduct.value = { name: "", price: 0 };
    }
  });
  const getProducts = async () => {
    loadingStore.dialog = true;
    try {
      const res = await productService.getProducts();
      products.value = res.data;
      loadingStore.dialog = false;
    } catch (err) {
      console.log(err);
      messageStore.showError("Cannot get products");
    }

    loadingStore.dialog = false;
  };

  const saveProduct = async () => {
    loadingStore.dialog = true;

    if (editProduct.value.id) {
      try {
        const res = await productService.updateProduct(
          editProduct.value.id,
          editProduct.value
        );
      } catch (err) {
        console.log(err);
      messageStore.showError("Cannot update products");

      }
    } else {
      try {
        const res = await productService.saveProduct(editProduct.value);
        dialog.value = false;
      } catch (err) {
        console.log(err);
      messageStore.showError("Cannot save products");

      }
    }
    await getProducts();
    loadingStore.dialog = false;
  };
  const EditProduct = (product: Product) => {
    editProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  };

  const deleteProduct = async (id: number) => {
    loadingStore.dialog = true;

    try {
      const res = await productService.deleteProduct(id);
      await getProducts();
    } catch (err) {
      console.log(err);
      messageStore.showError("Cannot delete products");

    }
    loadingStore.dialog = false;
  };

  return {
    deleteProduct,
    EditProduct,
    saveProduct,
    editProduct,
    products,
    getProducts,
    dialog,
  };
});
