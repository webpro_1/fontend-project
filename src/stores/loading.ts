import { defineStore } from "pinia"
import { ref } from "vue"

export const useLoadingStore = defineStore('loading', () => {
    const dialog = ref(false);

    return{dialog}

})