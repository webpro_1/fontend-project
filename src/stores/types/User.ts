export default interface User {
    id?: number;
    name: string;
    login: string;
    password: string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date ;
  }
  